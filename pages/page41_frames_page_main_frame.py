from page_objects import PageObject, PageElement

class MainFramePage(PageObject):

    def check_page(self):
        return "main" in self.w.title
