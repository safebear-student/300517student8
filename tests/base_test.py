import unittest
import time
from utils import Parameters
from page10_welcome_page import WelcomePage
from page20_login_page import SignInPage
from page30_main_page import MainPage
from page40_frames_page import FramesPage


class BaseTest(unittest.TestCase):
    param = Parameters()

    WelcomePage = WelcomePage(param.w, param.rootUrl)
    SignInPage = SignInPage(param.w, param.rootUrl)
    MainPage = MainPage(param.w, param.rootUrl)
    FramesPage = FramesPage(param.w, param.rootUrl)

    def setUp(self):
        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.WelcomePage.check_page()

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.param.w.quit()

