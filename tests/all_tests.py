import base_test
class TestCases(base_test.BaseTest):

    def test_01_test_login_page_login(self):
        #Step 1: Click on Login and the Login Page Loads
        assert self.WelcomePage.click_login(self.SignInPage)

        #Step2: Login to the webpage and confirm the main page appears
        assert self.SignInPage.login(self.MainPage, "testuser", "testing")

        #Step3: Logout of the webpage
        assert self.MainPage.click_logout(self.WelcomePage)
